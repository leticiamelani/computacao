 #include <stdio.h>
#include <stdlib.h>
#include<math.h>
#define FI 0

int main()
{
    float A,m,t,K,w,x,v,a,k,U;
    char aspas = '"';
    int i=0;
    int opcao;

    FILE *fd = NULL;

    printf("VALORES SUGERIDOS: A: 0.1 a 1, t: 100 a 500, m: 0.1 a 1, k: 200 a 400");

    printf("\nDigite o valor da amplitude (em m):");
    scanf("%f",&A);

    printf("\nDigite o tempo final (em s):");
    scanf("%f",&t);

    printf("\nDigite o valor da massa (em Kg):");
    scanf("%f",&m);

    printf("\nDigite o valor da constante elástica (K=N/m):");
    scanf("%f",&K);

do{
    printf("PROPRIEDADE DAS ONDAS:\n");
    printf("1 - Grafico: Deslocamento x tempo\n");
    printf("2 - Grafico: Velocidade x tempo\n");
    printf("3 - Grafico: Aceleracao x tempo\n");
    printf("4 - Sobreposicao de Graficos\n");
    printf("ENERGIA:\n");
    printf("5 - Grafico: Energia cinetica\n");
    printf("6 - Grafico: Energia potencial\n");
    printf("7 - Sobreposicao de Graficos\n");
    printf("8 - DADOS:\n");
    printf("\n");
    scanf("%i",&opcao);

    switch (opcao)
    {
    case 1 :


    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\deslocamento.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    w= sqrt((K/m));

    for(i=0;i<t;i++){
    x= A*sin(w*i+FI);
    fprintf(fd,"%i\t%f\n",i,x);
    i+ 0.0001;
        }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scriptx.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\deslocamento.txt' with lines linetype 2\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c x(t) %c \n",aspas,aspas);
    fprintf(fd,"set title %c Deslocamento x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scriptx.txt");

    break;

    case 2:

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\velocidade.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    w= sqrt((K/m));

    for(i=0;i<t;i++){
    v= A*w*cos(w*i+FI);
    fprintf(fd,"%i\t%f\n",i,v);
    i+ 0.0001;
        }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scriptv.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}

    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\velocidade.txt' with lines linetype 21\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c v(t) %c \n",aspas,aspas);
    fprintf(fd,"set title %c Velocidade x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scriptv.txt");

    break;

    case 3:
        if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\aceleracao.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    w = sqrt((K/m));

    for(i=0;i<t;i++){
    a = -(A*pow(w,2)*sin(w*i+FI));
    fprintf(fd,"%i\t%f\n",i,a);
    i+ 0.001;
        }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scripta.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\aceleracao.txt' with lines linetype 9\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c a(t) %c \n",aspas,aspas);
    fprintf(fd,"set title %c Aceleração x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scripta.txt");

    break;

    case 4:

        if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\sobreposicao.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scripts.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\aceleracao.txt' with lines linetype 9, 'C:\\Users\\letic\\Desktop\\velocidade.txt' with lines linetype 14,'C:\\Users\\letic\\Desktop\\deslocamento.txt' with lines linetype 16\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c a(t), v(t), x(t) %c \n",aspas,aspas);
    fprintf(fd,"set title %c Sobreposiçao %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scripts.txt");

    break;

    case 5 :

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\cinetica.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    w= sqrt((K/m));

    for(i=0;i<t;i++){
    k= 0.5*(m*pow(w,2)*pow(A,2)*pow(cos(w*i),2));
    fprintf(fd,"%i\t%f\n",i,k);
    i+ 0.0001;
        }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scriptc.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}

    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\cinetica.txt' with lines linetype 4\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c Energia cinética %c \n",aspas,aspas);
    fprintf(fd,"set title %c Energia cinética x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scriptc.txt");

    break;

    case 6:
        if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\potencial.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}
    w= sqrt((K/m));

    for(i=0;i<t;i++){

    U =0.5*(m*pow(w,2)*pow(A,2)*pow(sin(w*i),2));
    fprintf(fd,"%i\t%f\n",i,U);
    i+ 0.0001;
        }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scriptp.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}

    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\potencial.txt' with lines linetype 1\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c Energia potencial %c \n",aspas,aspas);
    fprintf(fd,"set title %c Energia potencial x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scriptp.txt");

    break;

    case 7:

     if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\total.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");

    }

    fclose(fd);

    if ( ( fd = fopen("C:\\Users\\letic\\Desktop\\scriptto.txt","w")) == NULL ){
     printf("Erro ao abrir o arquivo!");
}

    fprintf(fd,"set grid \n");
    fprintf(fd,"plot 'C:\\Users\\letic\\Desktop\\cinetica.txt' with lines linetype 1,'C:\\Users\\letic\\Desktop\\potencial.txt' with lines linetype 18\n");
    fprintf(fd,"set xlabel %c Tempo %c \n",aspas,aspas);
    fprintf(fd,"set ylabel %c Energia potencial e cinética %c \n",aspas,aspas);
    fprintf(fd,"set title %c Energia x tempo %c \n",aspas,aspas);

    fclose(fd);
    system("C:\\Users\\letic\\Desktop\\gnuplot\\bin\\gnuplot -p C:\\Users\\letic\\Desktop\\scriptto.txt");

    break;

    case 8:

    w= sqrt((K/m));

    printf("Frequencia angular: %.2f rad/s\n",w);
    printf("Deslocamento: %.2f m\n",A*sin(w*t+FI));
    printf("Aceleracao: %.2f m/s^2\n",-(A*pow(w,2)*sin(w*t+FI)));
    printf("Velocidade: %.2f m/s\n", A*w*cos(w*t+FI));
    printf("Energia cinetica: %.2f J\n",0.5*(m*pow(w,2)*pow(A,2)*pow(cos(w*t),2)));
    printf("Energia potencial: %.2f J\n",0.5*(m*pow(w,2)*pow(A,2)*pow(sin(w*t),2)));



    default:
    printf("Opcao invalida\n");

    }
    }while(opcao!=0);


    return 0;
}